---
layout: post
title:  "Deutz - Marelli 66 kVA"
date:   2013-11-15 12:30:00
category: Energoglobal
imgs:
  - 03/deutz_marelli_66_kva_1.jpg
  - 03/deutz_marelli_66_kva_2.jpg
  - 03/deutz_marelli_66_kva_3.jpg
  - 03/deutz_marelli_66_kva_4.jpg
cena: na upit
excerpt: Agregat snage 66 kVA, 1350 radnih sati. Motor Deutz, generator Marelli.
---

### Osnovni podaci
* Stand by snaga [kVA]: 66
* Stand by snaga [kW]: 53
* Prime snage [kVA]: 60
* Prime snage [kW]: 48
* Broj radnih sati: 1350

### Motor
* Proizovđač: Deutz
* Model: F6L912
* Broj obrtaja [RPM]: 1500

### Generator
* Proizovđač: Marelli
* Broj polova: 4
