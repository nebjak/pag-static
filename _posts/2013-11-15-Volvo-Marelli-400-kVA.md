---
layout: post
title:  "Volvo - Marelli 400 kVA"
date:   2013-11-15 12:00:00
category: Energoglobal
imgs:
  - 01/volvo_marelli_400_kva_1.jpg
  - 01/volvo_marelli_400_kva_2.jpg
  - 01/volvo_marelli_400_kva_3.jpg
  - 01/volvo_marelli_400_kva_4.jpg
  - 01/volvo_marelli_400_kva_5.jpg
  - 01/volvo_marelli_400_kva_6.jpg
cena: na upit
excerpt: Agregat snage 400 kVA, 1600 radnih sati. Motor Volvo, generator Marelli.
---

### Osnovni podaci
* Stand by snaga [kVA]: 400
* Stand by snaga [kW]: 320
* Prime snage [kVA]: 364
* Prime snage [kW]: 290
* Broj radnih sati: 1600

### Motor
* Proizovđač: Vovlo
* Model: TAD1232GE
* Broj obrtaja [RPM]: 1500

### Generator
* Proizovđač: Marelli
* Broj polova: 4
