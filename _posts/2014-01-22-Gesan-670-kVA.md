---
layout: post
title:  "Gesan DMS 670"
date:   2014-01-22 12:00:00
category: Energoglobal
imgs:
  - 14/gesan_670_kva_1.jpg
  - 14/gesan_670_kva_2.jpg
  - 14/gesan_670_kva_3.jpg
  - 14/gesan_670_kva_4.jpg
  - 14/gesan_670_kva_5.jpg
  - 14/gesan_670_kva_6.jpg
  - 14/gesan_670_kva_7.jpg
cena: na upit
excerpt: Agregat Gesan DMS 670, snage 670 kVA.
---

### Osnovni podaci
* Stand by snaga [kVA]: 670
* Stand by snaga [kW]: 536
* Prime snage [kVA]: 609
* Prime snage [kW]: 487
* Frekvencija [Hz]: 50
* cos φ: 0,8

### Motor
* Proizovđač: MAN
* Model: D2842 LE 203
* Broj obrtaja [RPM]: 1500

### Generator
* Proizvođač: Leroy-Somer
