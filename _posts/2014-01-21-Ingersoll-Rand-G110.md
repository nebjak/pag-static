---
layout: post
title:  "Ingersoll Rand G110"
date:   2014-01-21 12:00:00
category: Energoglobal
imgs:
  - 12/ingersoll_rand_g110_1.jpg
  - 12/ingersoll_rand_g110_2.jpg
  - 12/ingersoll_rand_g110_3.jpg
  - 12/ingersoll_rand_g110_4.jpg
cena: na upit
excerpt: Agregat snage 110 kVA, 2003. godište.
---

### Osnovni podaci
* Stand by snaga [kVA]: 110
* Stand by snaga [kW]: 88
* Prime snage [kVA]: 100
* Prime snage [kW]: 80
* Frekvencija [Hz]: 50
* cos φ: 0,8
* Godina proizvodnje: 2003.

### Motor
* Proizovđač: Cummins
* Broj obrtaja [RPM]: 1500

### Generator
* Proizvođač: Leroy-Somer
