---
layout: post
title:  "Rakovica - Končar 25 kVA"
date:   2014-01-28 17:30:00
category: Energoglobal
imgs:
  - 17/rakovica_koncar_25_kva_1.jpg
  - 17/rakovica_koncar_25_kva_2.jpg
  - 17/rakovica_koncar_25_kva_3.jpg
  - 17/rakovica_koncar_25_kva_4.jpg
  - 17/rakovica_koncar_25_kva_5.jpg
  - 17/rakovica_koncar_25_kva_6.jpg
  - 17/rakovica_koncar_25_kva_7.jpg
  - 17/rakovica_koncar_25_kva_8.jpg
  - 17/rakovica_koncar_25_kva_9.jpg
  - 17/rakovica_koncar_25_kva_10.jpg
  - 17/rakovica_koncar_25_kva_11.jpg
cena: na upit
excerpt: Agregat snage 25 kVA. Motor Rakovica, generator Končar.
---

### Osnovni podaci
* Prime snage [kVA]: 25
* Prime snage [kW]: 20
* Frekvencija [Hz]: 50
* cos φ: 0,8

### Motor
* Proizovđač: Rakovica
* Model: M 34

### Generator
* Proizovđač: Končar
* Model: 4S 200L4-8-4-C,F
* Snaga [kVA]: 25
* Struja [A]: 36,1
