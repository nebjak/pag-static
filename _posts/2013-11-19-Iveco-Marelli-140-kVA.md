---
layout: post
title:  "Iveco - Marelli 140 kVA"
date:   2013-11-19 12:00:00
category: Energoglobal
imgs:
  - 05/iveco_marelli_140_kva_1.jpg
  - 05/iveco_marelli_140_kva_2.jpg
  - 05/iveco_marelli_140_kva_3.jpg
  - 05/iveco_marelli_140_kva_4.jpg
  - 05/iveco_marelli_140_kva_5.jpg
  - 05/iveco_marelli_140_kva_6.jpg
cena: na upit
excerpt: Agregat snage 140 kVA, 2250 radnih sati. Motor Iveco, generator Marelli.
---

### Osnovni podaci
* Stand by snaga [kVA]: 140
* Stand by snaga [kW]: 112
* Prime snage [kVA]: 127
* Prime snage [kW]: 102
* Broj radnih sati: 2250

### Motor
* Proizovđač: Iveco
* Broj obrtaja [RPM]: 1500

### Generator
* Proizovđač: Marelli
* Broj polova: 4
