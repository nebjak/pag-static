---
layout: post
title:  "Hitzinger SGB 602/4"
date:   2013-11-19 12:30:00
category: Energoglobal
imgs:
  - 06/hitzinger_sgb_602_4_1.jpg
  - 06/hitzinger_sgb_602_4_2.jpg
  - 06/hitzinger_sgb_602_4_3.jpg
  - 06/hitzinger_sgb_602_4_4.jpg
  - 06/hitzinger_sgb_602_4_5.jpg
  - 06/hitzinger_sgb_602_4_6.jpg
cena: na upit
excerpt: Agregat snage 250 kVA. Motor MAN, generator Hitzinger.
---

### Osnovni podaci
* Proizvođač: Hitzinger
* Model: SGB 602/4
* Stand by snaga [kVA]: 250
* Stand by snaga [kW]: 200
* Prime snage [kVA]: 230
* Prime snage [kW]: 184
* Frekvencija [Hz]: 50
* cos φ: 0,8

### Motor
* Proizovđač: MAN
* Model: D 2530 MTE
* Broj obrtaja [RPM]: 1500

### Generator
* Proizovđač: Hitzinger
* Model: SGB 602/4
* Snaga [kVA]: 250
* Struja [A]: 360
* Broj polova: 4
* Zaštita: IP21
