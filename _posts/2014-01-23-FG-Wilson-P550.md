---
layout: post
title:  "FG Wilson P550"
date:   2014-01-23 12:00:00
category: Energoglobal
imgs:
  - 15/fg_wilson_p550_1.jpg
  - 15/fg_wilson_p550_2.jpg
  - 15/fg_wilson_p550_3.jpg
  - 15/fg_wilson_p550_4.jpg
  - 15/fg_wilson_p550_5.jpg
  - 15/fg_wilson_p550_6.jpg
  - 15/fg_wilson_p550_7.jpg
cena: na upit
excerpt: Agregat FG Wilson P550, snage 550 kVA.
---

### Osnovni podaci
* Stand by snaga [kVA]: 550
* Stand by snaga [kW]: 440
* Prime snage [kVA]: 500
* Prime snage [kW]: 400
* Frekvencija [Hz]: 50
* cos φ: 0,8

### Motor
* Proizovđač: Perkins
* Model: 3008-TAG4
* Broj obrtaja [RPM]: 1500
* Broj cilindara: 8

